+Tên biến:
-bắt đầu bằng chữ, $ hoặc _
vd: var fullName = 'Thai';
+comment: 
-Trên 1 dòng: //
-Nhiều dòng: /*  */
-Mục đích: ghi chú và vô hiệu hóa code
-Cách sử dụng phím tắt: crtl + /
+Hàm built-in cơ bản:
-alert
-console
-comfirm
-prompt: comfirm + ô nhập
-setTimeout thực thi 1 đoạn code sau 1 khoảng thời gian đặt ra
-setInterval: lặp lại 
+các kiểu dữ liệu:
-Nguyên thủy:number, string, boolean, undefine
-Phức tạp: function, object
+So sánh:
-ví dụ: 
	var a = 5;
	var b = 5;
	var c = '5' 
	a == b true 
	a == c true : chỉ xét kiểu dữ liệu
	a === c false : xét cả giá trị là kiểu dữ liệu
+truthy và falsy: 
-Truthy - to bool is true
Bất cứ giá trị nào trong Javascript khi chuyển đổi sang kiểu dữ liệu boolean mà có giá trị true thì ta gọi giá trị đó là Truthy.
Các giá trị 1, ['BMW'], { name: 'Miu' } và 'hi' được đề cập trong ví dụ dưới đây là Truthy vì khi chuyển sang Boolean ta nhận được giá trị true.
Ví dụ:
	console.log(Boolean(1)) // true
	console.log(Boolean(['BMW'])) // true
	console.log(Boolean({ name: 'Miu' })) // true
	console.log(!!'hi') // true
-Falsy - to bool is false
Bất cứ giá trị nào trong Javascript khi chuyển đổi sang kiểu dữ liệu boolean mà có giá trị false thì ta gọi giá trị đó là Falsy.
Trong Javascript có 6 giá trị sau được coi là Falsy:
	false
	0 (số không)
	'' or "" (chuỗi rỗng)
	null
	undefined
	NaN
Ví dụ:
	console.log(!!false) // false
	console.log(!!0) // false
	console.log(!!'') // false
	console.log(!!null) // false
	console.log(!!undefined) // false
	console.log(!!NaN) // false
+Chuỗi:
-template string:

ví dụ: 
	var firstName = 'Thai';
	var lastName = 'Tu';
	console.log(`Toi la: ${lastName} ${firstName}`)
-s.length(), s.index(), s.search(), s.slice(a,b), s.replace('a', 'b'), s.split(', '), s.charAt(1),...
+Mảng (array object)
- a.pop(), a.push(), a.shift(), a.shift(), a.splicing(1,0,'hihi'), a.concat(b)
+Hàm
-Constructor, get, set
+Đối tượng date
+rẽ nhanh switch case
+vòng lặp:
-foreach
-some: duyệt toàn bộ, nếu có 1 cái đúng là trả về đúng
-every: duyệt xét điều kiện toàn bộ mảng, nếu có ít nhất 1 vị trí k thỏa mãn thì sẽ trả về false
cú pháp : array.every(function(a, index){
	return a.price = 0;0 
	});
-find: tìm 1 phân tử của mảng thỏa mãn điều kiện
cú pháp :b =  array.find(function(a, index){
	return a.price = 0;0 
	});
-filter: giống find: trả về toàn bộ phần tử thỏa mãn
-map: var newarray = oldarray.map(funtion(a){
		return abc;	
	});
+ duyệt qua tất cả cả phần từ và trả lại các thứ mình muốn, hoặc thêm những thứ mới để trả về mảng mới
-reduce: ............
-include (dùng cho string và mảng):
+ ví dụ: var courses = ['Javascript', 'PHP', 'Dart'];
	courses.includes('PHP') -> true
	courses.includes('PHP', 0); true  
+polyfil: đoạn code để cho phép các công nghệ mới chạy trên các trình duyệt cũ
II,
DOM: Document Object Model: mô hình các đối tượng trong tài liệu HTML. 
-Có 3 thành phần: elements, attribute, text
-DOM có thể lấy, thay đổi, xóa HTML elements
-DOM là quy chuẩn của w3 đưa ra, js là công cụ để truy xuất vào DOM

-Document Object: là tất cả mọi thứ của website
+cách lấy ra 1 thẻ trong DOM qua: id, class, tag, CSS selertor, HTML colletion
-getElementById('heading') trả về 1 element
-getElemetsByClassName('heading') trả về HTML Collection
-getElemetsByTagName('h1') trả về HTML Collection
-QuerySelector('.heading') trả về 1 element
-QuerySelectorAll('.heading): trả về NoteList
+Attribute: tất cả thuộc tính nằm trong thẻ element
+text note: tất cả chữ trong thẻ element
-innerText: trả về nội dung text thấy trên trình duyệt
-TextContent: trả về tất cả nội dung text ở trong DOM, kể cả các text ẩn và các nội dung nằm trong element
+InnerHTML: thêm 1 element b vào 1 element a
-khi thực hiện set thì nó nó sẽ ghi đè thẻ b lên thẻ c(nằm sẵn trong a) 
-get thì sẽ trả lại tất cả các thẻ nằm trong a 
+outerHTML:
-khi get sẽ trả lại cả thẻ a và các thẻ con của a
-khi set thì sẽ ghi đè thẻ a
+Dom CSS
+ClassList property: trả về mảng gồm các class có trong thẻ
- add: element.classList.add('red'): red là 1 class, thêm class red vào thẻ
- contains: element.classList.contains('red') kiểm tra xem thẻ có class hay k
-remove: xóa class trong thẻ
+DOM events:
III
+JSON: là một địngh dạng dữ liệu (chuỗi),
-stringify: chuyển đổi Js types -> json
ví dụ: JSON.strigify(['JS','PHP'])   -- > ["js","PHP"]
-parse: từ json -> js types
+Promise:
-Synchronous: xử lý đồng bộ: chương trình sẽ chạy theo từng bước và chỉ khi nào bước 1
thực hiện xong thì mới nhảy sang bước 2, khi nào chương trình này chạy xong mới nhảy qua 
chương trình khác. Đây là nguyên tắc cơ bản trong lập trình mà bạn đã được học đó là khi
biên dịch các đoạn mã thì trình biên dịch sẽ biên dịch theo thứ tự từ trên xuống dưới, 
từ trái qua phải và chỉ khi nào biên dịch xong dòng thứ nhât mới nhảy sang dòng thứ hai, 
điều này sẽ sinh ra một trạng thái ta hay gọi là trạng thái chờ
*Ưu điểm: Chương trình sẽ chạy theo đúng thứ tự và có nguyên tắc
*Nhược điểm:Chương trình chạy theo thứ tự đồng bộ nên sẽ sinh ra trạng thái chờ và là không
cần thiết trong một số trường hợp, lúc này bộ nhớ sẽ dễ bị tràn vì phải lưu trữ các trạng thái chờ vô duyên đó.
-Asynchronous là xử lý bất động bộ, nghĩa là chương trình có thể nhảy đi bỏ qua một bước nào đó,
vì vậy Asynchronous được ví như một chương trình hoạt động không chặt chẽ và không có quy trình nên việc quản lý rất khó khăn. Nếu một hàm A phải bắt buộc chạy trước hàm B thì với Asynchronous sẽ không thể đảm bảo nguyên tắc này luôn đúng.
*Ưu điểm: Có thể xử lý nhiều công việc một lúc mà không cần phải chờ đợi nên tạo cảm giác thoải mái 
*Nhược điểm:Nếu một chuong trình đòi hỏi phải có quy trình thì bạn không thể sử dụng Asynchronous được
-Ajax Asynchronous:
-cú pháp promise: 
	var promise = new promise(
		funtion(resolove, reject){
		//.....Logic
		// Thành công: resolve()
		// Thất bại: reject();
	})
	promise 
		.then(funtion(){
			console.log('Thanh cong');
		});
		.catch(funtion(){
			console.log('That bai');
		});
		.finnally(funtion(){
			console.log('Thanh cong');
		});



