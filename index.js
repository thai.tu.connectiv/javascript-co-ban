/* var courses = [ 
    {
        id: 1,
        name: 'javascript',
        coin: 100
    },
    { 
        id: 2,
        name: 'HTML, CSS',
        coin: 0
    },
    { 
        id: 3,
        name: 'RuBy',
        coin: 200
    },
    { 
        id: 4,
        name: 'PHP',
        coin: 200
    },
    { 
        id: 5,
        name: 'REACT',
        coin: 300
    }
];
var totalCoin = courses.reduce(function(total, course){
    return total + course.coin;
}, 0);
// console.log(totalCoin);
var topics = [
  {
    topic: "fontEnd",
    courses:  [
        {
            id: 1,
            title: "HTML,CSS"
        },
        {
            id: 2,
            title: "Javascript"
        }
    ]
  } ,
  {
    topic: "backEnd",
    courses:  [
        {
            id: 3,
            title: "PHP"
        },
        {
            id: 4,
            title: "notejs"
        }
    ]
  } 
];
var newCourses = topics.reduce(function(courses,topic){
    return courses.concat(topic.courses);
}, []);
// console.log(newCourses);

var htmls = newCourses.map(function(course){
    return `
        <div>
                <h2>${course.title}</h2>
                <p> ID : ${course.id}</p>

        </div>

    `;
});
console.log(htmls);
*/


//------------- viết map
/*
Array.prototype.map2 = function(callBack){
    var listCourse = [];
    var arrayLength = this.length;
    for(var i = 0; i < arrayLength; ++i ){
       var kq = callBack(this[i], i);
        listCourse.push(kq);
    }
    return listCourse;
}
var courses = [
    'Javascript',
    'PHP',
    'Ruby'
];
var  getInfo = function(course) {
    return `<h2>${course}</h2>`;
        
};
var lish = courses.map2(getInfo);

console.log(lish);
/* var htmls = courses.map(function(course){
    return course;
});
console.log(htmls);
*/

// Viết foreach

// var courses = [
//     'Javascript',
//     'PHP',
//     'Ruby'
// ];
//  /*
// courses.length = 100;
// console.log(courses);
// courses.forEach(function(course, index, array){
//     console.log(course, index, array);
// });
// */
// Array.prototype.forEach2 = function(callBack){
//     for( var index in this){
//         if(this.hasOwnProperty(index)){
//             callBack(this[index], index, this);
//         }
//     }
// }
// var callBack = function(course, index, array){
//     console.log(course, index, array);
// }
// courses.forEach2((callBack));
// Viết filter
/*
Array.prototype.filter2 = function(callBack){
    var outPut = [];
    for(var index in this){ 
        if(this.hasOwnProperty(index)){
            var  kq = callBack(this[index], index);
            if(kq){
                outPut.push(this[index]);
            }
        }
    }
    return outPut;
};
var courses = [
    {
        name: 'Javascript',
        coin: 680

    },
    {
        name: 'PHP',
        coin: 860
    },
    {
        name: 'PHP',
        coin: 980
    }
];

var callBack = function(course, index){
        return course.coin > 700;
} ;

var filterCourses = courses.filter2(callBack);
console.log(filterCourses);
*/
//----------------
/*
// Thay đổi attribute qua js đi từ elemnent
  var headingElement = document.querySelector('h1');
//  console.log(headingElement);
  headingElement.title = 'Heading';

  console.log(headingElement.getAttribute('title'));
  */
//---------------
// Thêm text và sửa text
/* 
 var headingElement = document.querySelector('h1');
console.log(headingElement.innerText);
headingElement.innerText = 'new heading';
console.log(headingElement.innerText);
*/
/* 
var boxElemnt = document.querySelector('.box');
//console.log(boxElemnt);
boxElemnt.innerHTML = '<span>HIHI</span>'
console.log(boxElemnt.outerHTML);
*/
/* var boxElement = document.querySelector('.box');
// boxElement.style.width = '100px';
// boxElement.style.height = '100px';
// boxElement.style.backgroudColor = 'red';
console.log(boxElement.classList);
*/
var ar = ["Nguyen", "Van", "Quan"];
//console.log(JSON.stringify(ar));
var json = ' { "name": "tu thai", "age": 18 }';
var ob = JSON.parse(json);
//console.log(ob);


// Promise
var promise = new Promise(
    function(resolve, reject) {
        //logic
        // Thành công: resolvez()
        //That bai : reject()
        reject();
    }
);
promise
    .then(function() {
        console.log('Successful !');
    })
    .catch(function() {
        console.log('falirue!');
    })
    .then(function() {
        console.log('done !');
    })